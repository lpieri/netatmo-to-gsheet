#! /usr/bin/env python3

import netatmo
import gspread
from datetime import datetime

gc = gspread.service_account(filename=os.path.dirname(__file__) + '/key.json')
wks = gc.open("temperatures").sheet1

ws = netatmo.WeatherStation( {
        'client_id': '***',
        'client_secret': '***',
        'username': 'user@example.com',
        'password': 'p@ssw0rd' } )
ws.get_data()

devices = ws.devices

for device in devices:
    wks.append_row(list(device['dashboard_data'].values()))
