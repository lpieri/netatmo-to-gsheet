## Setup

Follow [these instructions](https://gspread.readthedocs.io/en/latest/oauth2.html#for-bots-using-service-account) to setup authentication to Google spreadsheet API.

```sh
pip3 install -r requirements.txt
```

## Usage

```sh
python3 netatmo-to-gsheet.py
```

## Acknowledgements

 - Netatmo [rene-d/netatmo](https://github.com/rene-d/netatmo)
 - Gspread [burnash/gspread](https://github.com/burnash/gspread)